var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var mongoose = require('mongoose');
const bodyparser = require("body-parser");


var app = express();
//routers
var employeeRouter = require('./routes/route.employee');

//mongodb configuration
const db = require('./connect').mongoURL;
//attempt to connect to databse
mongoose
    .connect(db)
    .then(() => console.log('MongoDB connected successfully'))
    .catch(err => console.log(err));


app.get('/', (req, res) => {

    res.sendFile(__dirname + '/index.html')
    res.render("index");
});




app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug")


// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/employee', employeeRouter);


app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.json({ error: err.status });
});

app.listen(3000, () => console.log("server is running at port 3000.."));