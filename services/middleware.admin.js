var express = require('express');
const { check, validationResult } = require('express-validator');

const Validator = require('validator');
const isEmpty = require('./is-empty');
const modelEmployee = require('../models/model.employee');

//function
module.exports = validateLoginInput(employee => {
    let errors = {};
    employee.userName = !isEmpty(employee.name) ? employee.userName : '';
    employee.password = !isEmpty(employee.password) ? employee.password : '';

});




if (Validator.isEmpty(employee.userName)) {
    errors.userName = 'Name field is required';
}
if (Validator.isEmpty(employee.password)) {
    errors.password = 'Password is required';
}
return {
    errors,
    isValid: isEmpty(errors)
}