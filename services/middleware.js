var express = require('express');
const { check, validationResult } = require('express-validator');
module.exports={

updateFieldsCheck:(req,res,next)=>{

    if(req.session.category=="C"){
    [
        check('address').not().isEmpty().trim().escape(),
        check('panNo').not().isEmpty().trim().escape(),
        check('clientSource').not().isEmpty().trim().escape(),

        // check('entiryRegNumber').not().isEmpty().trim().escape(),
        // check('gstNo').not().isEmpty().trim().escape(),
        // check('dinNo').not().isEmpty().trim().escape(),
        // check('ptNo').not().isEmpty().trim().escape(),
        // check('pf_esiNo').not().isEmpty().trim().escape(),
        // check('iecNo').not().isEmpty().trim().escape(),    
    ]

    }
    else if(req.session.category=="G"){
        [
            check('address').not().isEmpty().trim().escape(),
            check('panNo').not().isEmpty().trim().escape(),
            check('clientSource').not().isEmpty().trim().escape(),

            check('ddoNo').not().isEmpty().trim().escape()
        ]

    }
    else if(req.session.category=="I"){
        [
            check('address').not().isEmpty().trim().escape(),
            check('panNo').not().isEmpty().trim().escape(),
            check('clientSource').not().isEmpty().trim().escape(),

            check('aadharNo').not().isEmpty().trim().escape(),
        ]

    }
    else if(req.session.category=="LP"){
        [
            check('address').not().isEmpty().trim().escape(),
            check('panNo').not().isEmpty().trim().escape(),
            check('clientSource').not().isEmpty().trim().escape(),

            check('entiryRegNumber').not().isEmpty().trim().escape(),
        ]

    }
    else if(req.session.category=="P"){
        [
            check('address').not().isEmpty().trim().escape(),
            check('panNo').not().isEmpty().trim().escape(),
            check('clientSource').not().isEmpty().trim().escape(),

            check('entiryRegNumber').not().isEmpty().trim().escape(),

            
        ]

    }
    else if(req.session.category=="PR"){
        [
            check('address').not().isEmpty().trim().escape(),
            check('panNo').not().isEmpty().trim().escape(),
            check('clientSource').not().isEmpty().trim().escape(),

            check('aadharNo').not().isEmpty().trim().escape(),
            
        ]

    }
    else if(req.session.category=="T"){
        [
            check('address').not().isEmpty().trim().escape(),
            check('panNo').not().isEmpty().trim().escape(),
            check('clientSource').not().isEmpty().trim().escape(),

        ]

    }
    else{
        res.status(401);
        res.send("Authorisation Error");
    }

    const errors=validationResult(req);
     if(errors.isEmpty()){
         next();
     }
     else{
        res.status(401);
        res.send("Authorisation Error");
     }


}



}