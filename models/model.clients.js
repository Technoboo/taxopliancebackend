var mongoose=require('mongoose');
var Schema=mongoose.Schema; 

var clients=new Schema({
    _id:String,
    fullName:{
        type:String,
        required:true
    },
    category:{
        type:String,
        required:true
    },
    email:{
       type:String,
       required:true,
       unique:true
    },
    password:{
       type:String,
       required:true
    },
    phoneNo:{
        type:String,
        required:true
    },
    flag:{
        type:String,
        default:null
    },

    profile:{
        contact:{
            address:{
                type:String
            },
            panNo:{
                type:String
            },
    
            aadharNo:{
                type:Number
            },
            clientSource:{
                type:String
            }

        },
        registration:{
            entiryRegNumber:{
                type:String
            },
            gstNo:{
                type:String
            },
            dinNo:{
                type:String
            },
            ptNo:{
                type:String
            },
            pf_esiNo:{
                type:String
            },
            iecNo:{
                type:String
            },
            ddoNo:{
                type:String
            }
        }

    }


});


module.exports=mongoose.model('clients',clients);
