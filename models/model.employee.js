var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var employees = new Schema({
    _id: String,
    fullName: String,
    userName: String,
    email: String,
    password: String,
    phoneNo: String,
    jobs: [String],
    authority: String,
    loginRecords: {
        loginTime: String,
        logoutTime: String
    }
});


module.exports = mongoose.model('employees', employees);