var mongoose=require('mongoose');
var Schema=mongoose.Schema; 

var jobs=new Schema({
    _id:String,
    startDate:String,
    endDate:String,
    title:String,
    description:String,
    cost:String,
    comments:{
        fullName:String,
        empId:String,
        comment:String

    },
    feePaid:Boolean
});


module.exports=mongoose.model('jobs',jobs);
