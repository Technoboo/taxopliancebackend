//schema for database
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const AdminSchema = new Schema({

    UserName: {
        type: String,
        required: true
    },
    Email: {
        type: String,
        required: true
    },

    password: {
        type: String,
        required: true
    }



});


module.exports = Admin = mongoose.model("admin", AdminSchema);