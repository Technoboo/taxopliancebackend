const express = require("express");
const bodyparser = require("body-parser");
var path = require("path");
var mongoose = require("mongoose");
const passport = require("passport");
var bcrypt = require("bcryptjs");


const app = express();
//bring all routes
app.use(require('./routes/auth'))


//middleware for bodyparser
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());



//mongodb configuration
const db = require('./connect').mongoURL;
//attempt to connect to databse
mongoose
    .connect(db)
    .then(() => console.log('MongoDB connected successfully'))
    .catch(err => console.log(err));

//Passport middleware
// app.use(passport.initialize());
// //config for jwt
// require("./strategies/jsonwtStrategy")(passport);

// //just for testing  -> route
// app.get("/", (req, res) => {
//     res.send("Hey there Big stack");
// });

//actual routes
const auth = require("./routes/auth");
app.use("/auth", auth);

const port = process.env.PORT || 3000;
app.use(bodyparser.urlencoded({ extended: false }));

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug")

// app.use("/", express.static(__dirname + "/public"));
app.get('/', (req, res) => {

    res.sendFile(__dirname + '/index.html')
    res.render("index");
});

//login
app.post("/login", (req, res) => {
    console.log(req.body);
    res.redirect('/')
    res.send("login success");

});








app.listen(3000, () => console.log("server is running at port 3000.."));