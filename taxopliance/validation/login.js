const Validator = require('validator');
const isEmpty = require('./is-empty');

//function
module.exports = validateLoginInput(Admin => {
    let errors = {};
    Admin.UserName = !isEmpty(Admin.name) ? Admin.UserName : '';
    Admin.password = !isEmpty(Admin.password) ? Admin.password : '';

});

if (Validator.isEmpty(admin.UserName)) {
    errors.UserName = 'Name field is required';
}
if (Validator.isEmpty(admin.password)) {
    errors.password = 'Password is required';
}
return {
    errors,
    isValid: isEmpty(errors)
}