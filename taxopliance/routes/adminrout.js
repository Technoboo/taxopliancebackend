var express = require('express');
var router = express.Router();
const { check, validationResult } = require('express-validator');
var adminController = require('../controllers/admincontroller');

router.get('/login', function(req, res, next) {
    res.send('Admin Login ');
});



router.post('/login', [check('username').isUserName(),
    check('password').not().isEmpty().trim().escape()
], adminController.login);




module.exports = router;