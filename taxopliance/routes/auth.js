const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jsonwt = require("jsonwebtoken");
const key = require("../connect");


//import schema for person to login
const Admin = require("../models/admin");


router.post('/login', (req, res) => {




    const username = req.body.UserName;
    const password = req.body.password;


    Admin.findOne({ username })
        .then(admin => {
            if (!admin) {
                return res
                    .status(404)
                    .json({ usernameerror: 'admin not found' });

            }
            bcrypt
                .compare(password, admin.password)
                .then(isCorrect => {
                    if (isCorrect) {
                        // res.json({ success: "user is able to login with success" });
                        //payload for tokens
                        const payload = {
                            username: admin.UserName

                        };
                        jsonwt.sign(
                            payload,
                            key.secret, { expiresIn: 3600 },
                            (err, token) => {
                                res.json({
                                    success: true,
                                    token: "Bearer " + token
                                });
                            }
                        );
                    } else {
                        res.status(400).json({ passworderror: "Password is not correct" });
                    }
                })
                .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
});





module.exports = router;