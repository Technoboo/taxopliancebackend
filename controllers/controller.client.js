var express = require('express');
const { check, validationResult } = require('express-validator');
var clientdb = require('../models/model.clients');

function newId(category) {
    let randomNum = Math.random() * ((new Date() / 1000) - 1) + 1;
    return category + "-" + Math.floor(randomNum + (new Date() / 1000));
}



module.exports = {

    checkClientLoggedIn: function(req, res, next) {
        if (req.session.loggedin)
            next();
        else
            res.render('login', { msg: "Please Login In First" });
    },


    register: function(req, res) {
        const errors = validationResult(req);
        if (errors.isEmpty()) {
            console.log(req.body);
            var category = req.body.category;
            var email = req.body.email;
            var password = req.body.password;
            var phoneno = req.body.phoneno;
            var fullname = req.body.fullname;

            console.log(category);
            var client = new clientdb({
                _id: newId(category),
                fullName: fullname,
                email: email,
                password: password,
                phoneNo: phoneno,
                category: category
            });

            client.save((err, resu) => {
                console.log(resu);
                if (err) {
                    console.log("Error: " + err);
                    res.status(500);
                    res.json({ Error: "Database Error" });
                } else
                    res.json({ Success: "Registered" });

            });

        } else {
            console.log(req.body);
            res.json({
                status: false,
                message: "Form Validation Error",
                errors: errors.array()
            });
        }

    },


    login: function(req, res) {
        const errors = validationResult(req);
        if (errors.isEmpty()) {
            var email = req.body.email;
            var password = req.body.password;

            clientdb.findOne({ email: email }, (err, client) => {
                if (err) {
                    console.log(err);
                    res.status(401);
                    res.json({ Error: "Authentication Failed" });
                } else {
                    console.log(client);
                    if (client.password == password) {
                        req.session.id = client._id;
                        req.session.name = client.fullName;
                        req.session.category = client.category;
                        req.session.loggedin = true;
                        res.send("Logged In");

                    } else {
                        console.log(err);
                        res.status(401);
                        res.json({ Error: "Authentication Failed" });
                    }
                }
            });
        } else {
            res.json({
                status: false,
                message: "Form Validation Error",
                errors: errors.array()
            });
        }


    }




}