var express = require('express');
var router = express.Router();
const { check, validationResult } = require('express-validator');
var clientController=require('../controllers/controller.client');
/* GET users listing. */
router.get('/loginpage', function(req, res, next) {
  res.send('Client Logon Page');
});


router.post('/register',
[check('email').isEmail().normalizeEmail(),
check('password').not().isEmpty().trim().escape(),
check('repeatpassword').not().isEmpty().trim().escape(),
check('category').not().isEmpty().trim().escape(),
check('phoneno').not().isEmpty().trim().escape(),
check('fullname').not().isEmpty().trim().escape()
],clientController.register);

router.post('/login',[check('email').isEmail().normalizeEmail(),
check('password').not().isEmpty().trim().escape()],clientController.login);


router.post('/updateprofile',[check('email').isEmail().normalizeEmail(),
check('password').not().isEmpty().trim().escape(),
check('repeatpassword').not().isEmpty().trim().escape(),
check('category').not().isEmpty().trim().escape(),
check('phoneno').not().isEmpty().trim().escape(),
check('fullname').not().isEmpty().trim().escape()
])

module.exports = router;
